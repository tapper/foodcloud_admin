
import {Http,Headers,Response, RequestOptions} from "@angular/http";
import {Injectable} from "@angular/core";
import { HttpModule } from '@angular/http';
import 'rxjs/Rx';
import {MESSAGES} from "../email/mock-messages";
import { Message } from '../email/message';


const ServerUrl = "http://www.tapper.co.il/salecar/laravel/public/api/";
@Injectable()

export class CarService
{
    public headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    public options = new RequestOptions({headers:this.headers});

    public Companies:any[]=[];
    public CompanyArray:any[]=[];
    public Cars:any[]=[];

    constructor(private http:Http) { };
    
    getMessages(): Promise<Message[]> {
        return Promise.resolve(MESSAGES);
    }
    
    GetAllCars(url:string)
    {
        let body = new FormData();
        body.append('id','26');
        return this.http.post(ServerUrl + '' + url, body).map(res => res.json()).do((data)=>{this.Cars = data }).toPromise();
    }

    AddCompany(url,Company)
    {
        this.CompanyArray = Company;
        let body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    EditCompany(url,Company)
    {
        this.CompanyArray = Company;
        let body = 'company=' + JSON.stringify(this.CompanyArray);
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res).do((data)=>{}).toPromise();
    }

    DeleteCompany(url,Id)
    {
        let body = 'id=' + Id;
        return this.http.post(ServerUrl + '' + url, body, this.options).map(res => res.json()).do((data)=>{}).toPromise();
    }
};


